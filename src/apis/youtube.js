import axios from 'axios';

const KEY='AIzaSyBsGlN88AF2q5bjVHk19DdOa-k25KoA8fk';

export default axios.create({
    baseURL:'https://www.googleapis.com/youtube/v3',
    params:{
        part:'snippet',
        maxResults:5,
        key:KEY
    }
});

